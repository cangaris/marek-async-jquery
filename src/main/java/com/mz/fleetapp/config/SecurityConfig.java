package com.mz.fleetapp.config;

import com.mz.fleetapp.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserRepository userRepository;

    private static final String[] ALLOWED_URLS = {
//            "/h2-console/**",
            "/**",
            "/js/**",
            "/css/**",
            "/countries/edit/{id}",
//            "http://localhost:8080/**"
    };

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .csrf().disable(); // wyłączenie zabezpieczeń csrf
        http
            .cors().disable();
        http
                .authorizeRequests()
                .antMatchers(ALLOWED_URLS).permitAll();
        http
                .headers().frameOptions().disable();
    }
}
