package com.mz.fleetapp.controllers;

import com.mz.fleetapp.models.Country;
import com.mz.fleetapp.repositories.CountryRepository;
import com.mz.fleetapp.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public String getCountries(Model model) {
        List<Country> countryList = countryService.getCountries();
        model.addAttribute("countries", countryList);
        return "country";
    }

    @PostMapping("/countries/addNew")
    public String addNew(Country country) {
        countryService.save(country);
        return "redirect:/countries";
    }

    @GetMapping("/countries/remove/{id}")
    public String removeCountry(@PathVariable Integer id) {
        countryService.removeCountry(id);
        return "redirect:/countries";
    }
}
