package com.mz.fleetapp.controllers;

import com.mz.fleetapp.models.Country;
import com.mz.fleetapp.services.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CountryRestController {

    private final CountryService countryService;

    @GetMapping("/countries/edit/{id}")
    public ResponseEntity<Country> showEditCountryForm(@PathVariable Integer id) {
        var optionalCountry = countryService.findCountry(id);

        if (optionalCountry.isPresent()) {
            var country = optionalCountry.get();
            return ResponseEntity.ok(country);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/countries/edit/{id}")
    public void editCountry(@PathVariable Integer id, @RequestBody Country formCountry) {
        countryService.updateCountry(id, formCountry);
    }
}
