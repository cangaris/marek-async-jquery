package com.mz.fleetapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StateController {

    @PostMapping("/states")
    public String getStates() {
        return "state";
    }

}
