package com.mz.fleetapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class VehicleMaintenanceController {

    @GetMapping("/vehicleMaintenance")
    public String getVehicleMaintenances() {
        return "vehicleMaintenance";
    }

}
