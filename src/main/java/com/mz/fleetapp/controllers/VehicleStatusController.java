package com.mz.fleetapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class VehicleStatusController {

    @GetMapping("/vehicleStatuses")
    public String getVehicleStatuss() {
        return "vehicleStatus";
    }

}
