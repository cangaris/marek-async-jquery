package com.mz.fleetapp.repositories;

import com.mz.fleetapp.models.Client;
import com.mz.fleetapp.models.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
}
