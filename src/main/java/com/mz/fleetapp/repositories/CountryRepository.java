package com.mz.fleetapp.repositories;

import com.mz.fleetapp.models.Client;
import com.mz.fleetapp.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
}
