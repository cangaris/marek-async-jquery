package com.mz.fleetapp.repositories;

import com.mz.fleetapp.models.Client;
import com.mz.fleetapp.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
