package com.mz.fleetapp.services;


import com.mz.fleetapp.models.Country;
import com.mz.fleetapp.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    // Returns list of countries
    public List<Country> getCountries() {
        return countryRepository.findAll();
    }

    // Save new country
    public void save(Country country) {
        countryRepository.save(country);
    }

    // deleting of country
    public void removeCountry(Integer id) {
        countryRepository.deleteById(id);
    }

    public void updateCountry(Integer id, Country formCountry) {
        var optionalCountry = findCountry(id);
        if (optionalCountry.isPresent()) {
            var dbCountry = optionalCountry.get();
            dbCountry.setCapital(formCountry.getCapital());
            dbCountry.setNationality(formCountry.getNationality());
            dbCountry.setCode(formCountry.getCode());
            dbCountry.setContinent(formCountry.getContinent());
            countryRepository.save(dbCountry);
        }
    }

    public Optional<Country> findCountry(Integer id) {
        return countryRepository.findById(id);
    }
}
