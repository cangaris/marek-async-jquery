/**
 * capital: "Warsaw"
 code: "03278"
 continent: "Europe"
 nationality: "Poland"
 */
$('document').ready(function (){
    $('.editButton').click(function () {
        $('#addModal').modal('toggle');
        const id = $(this).attr('id');
        $.get( `/countries/edit/${id}`, function(data) {
            console.log(data);
            $('#addModal #code').val(data.code);
            $('#addModal #capital').val(data.capital);
            $('#addModal #continent').val(data.continent);
            $('#addModal #nationality').val(data.nationality);
        });
    });
    $( "#editForm" ).submit(function( event ) {
        event.preventDefault();
        const code = $('#addModal #code').val();
        const capital = $('#addModal #capital').val();
        const continent = $('#addModal #continent').val();
        const nationality = $('#addModal #nationality').val();

        $.ajax({
            type: "POST",
            url: "/countries/edit/33",
            data: JSON.stringify({ code, capital, continent, nationality }),
            complete: function( data ) {
                console.log('fdsfdsfds');
                $('#addModal').modal('toggle');
            },
            dataType: "json",
            contentType: "application/json",
        });
    });
});
